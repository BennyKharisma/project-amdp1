// login
function loginvalid(){
    var Email = document.getElementById("txtemail").value;
    var Password = document.getElementById("txtpass").value;

    if(Email == ''){
        alert("Email harus diisi");
    }
    if(Password == ''){
        alert("Password harus diisi");
    }
}

//register
function regisvalid(){
    var Name = document.getElementById("nametxt").value;
    var Email = document.getElementById("emailtxt").value;
    var Male = document.getElementById("radioMale");
    var Female = document.getElementById("radioFemale");
    var City = document.getElementById("citytxt").value;
    var Address = document.getElementById("addresstxt").value;

    if(Name == ''){
        alert("Nama harus diisi");
    }

    if(Email == ''){
        alert("Email harus diisi dan sesuai format email");
    }

    if(City == "..."){
        alert("City harus dipilih");
    }

    if((Male.checked == false && Female.checked == false) || Male.checked == true && Female.checked == true){
        alert("Gender harus dipilih salah satu");
    }

    var checkaddress = Address.includes('Street');

    if(Address == '' || checkaddress == false){
        alert("Address harus diisi dan mengandung kata 'Street'");
    }

}